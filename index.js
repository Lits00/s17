console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function getInfo(){
        let fullName = prompt("Please Enter your Full Name:");
        let age = prompt("How old are you? ");
        let location = prompt("Where do you live? ");
        console.log("Hello, " + fullName);
        console.log("You are " + age + " years old.");
        console.log("You live in " + location);
    }
    getInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function topFiveBands(){
        console.log("1. Air Supply");
        console.log("2. MLTR");
        console.log("3. Parokya Ni Edgar");
        console.log("4. Eraserheads");
        console.log("5. Ben&Ben");
    }
    topFiveBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function topFiveMovies(){
        console.log("1. Kimetsu No Yaiba - The Movie: Mugen Train");
        console.log("Rotten Tomatoes Rating: 98%");
        console.log("2. Your Name");
        console.log("Rotten Tomatoes Rating: 98%");
        console.log("3. Spider-Man: Into the Spider-Verse");
        console.log("Rotten Tomatoes Rating: 97%");
        console.log("4. One Piece: Stampede");
        console.log("Rotten Tomatoes Rating: 94%");
        console.log("5. El Camino: A Breaking Bad Movie");
        console.log("Rotten Tomatoes Rating: 92%");
    }
    topFiveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = /*function printUsers*/() => {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
